import turtle 
import pandas
import colorgram
screen = turtle.Screen()
screen.title("U.S. State Game")

image ="blank_states_img.gif"

screen.addshape(image)
turtle.shape(image)
states = pandas.read_csv("50_states.csv")

still_playing = True
correct_answers = 0
new_state_dict = states.to_dict()
guessed_correct_state = []
def find_key(val):
    for key,value in new_state_dict["state"].items():
        if value.lower() == val.lower():
            return key;

    return -1
    print("No key found")

while still_playing:

    answer_state = screen.textinput(title=f"{correct_answers}/{len(states)} correctly answered", prompt="what's another state's name?(Enter 'exit' to leave the program)").lower()
    if answer_state.lower() == 'exit':
        turtle.write("Exiting programm",align = "center",font = ('Arial',20,'normal'))
        break
    key_value = find_key(answer_state)
    if key_value == -1:
        continue
    elif answer_state in guessed_correct_state:
        continue
    else:
        guessed_correct_state.append(answer_state)
        correct_answers += 1
    new_address = turtle.Turtle()
    new_address.hideturtle()
    new_address.penup()
    new_address.goto(x=new_state_dict["x"].get(key_value), y=new_state_dict["y"].get(key_value))
    new_address.write(new_state_dict["state"][key_value],font = ('Arial',8,'normal'))
    if correct_answers == len(states)-1:
        still_playing = False
        continue
df=pandas.DataFrame(guessed_correct_state)
df.to_csv("Correctly_guessed_states.csv")
screen.mainloop()
